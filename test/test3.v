module test3 ( out ) ;
   output [2:0] out;
   reg [2:0] 	r;

   assign out = r;

   initial
	 begin

		r = 3'b0;

		repeat ('b111) begin
		   #1;
		   r++;
		end
	 end
endmodule // test3
