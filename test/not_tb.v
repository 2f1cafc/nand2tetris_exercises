module not_tb (/*AUTOARG*/ ) ;
   wire   t_y;
   reg    t_a;

   notgate mynot(.a(t_a), .y(t_y));

   initial begin
	  $monitor("| %d | %d |", t_a, t_y);

	  t_a = 1'b0;

	  #1

	  t_a = 1'b1;


   end

endmodule // not_tb
