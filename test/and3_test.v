/* testbench for AND gate
 File: and_tb.v */

module and3_test;
   wire t_y;
   wire [2:0] w1;

   test3 t3(w1);

   and3gate my_gate( .in(w1), .out(t_y));

   initial begin
	  $monitor("%3b || %b", w1, t_y);
   end

endmodule
