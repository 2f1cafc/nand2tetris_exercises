module add_carry_tb (/*AUTOARG*/ ) ;
   test3 tester(a, b, c);
   add_carry add(a,b,c, sum, cc);

   initial begin
	  $monitor(w, y);
   end

endmodule // add_carry_tb
