module notgate (a,y) ;

   input  a;
   output y;

   nand n(y, a, a);

endmodule // notgate
