module orgate (a, b, y ) ;
   input  a, b;
   output y;
   wire   nota, notb, neither;

   notgate n1(a, nota);
   notgate n2(b, notb);

   andgate a1(nota, notb, neither);

   notgate n3(neither, y);

endmodule // orgate
