module and3gate (in, out ) ;
   input  [2:0] in;
   output out;

   wire   w;

   andgate and1(in[0], in[1], w);
   andgate and2(in[2], w, out);

endmodule // and3gate
