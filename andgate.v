module andgate (a,b,y ) ;
   input  a, b;
   output y;
   wire   w;

   nand n1(w, a, b);
   notgate not1(w, y);

endmodule // andgate
