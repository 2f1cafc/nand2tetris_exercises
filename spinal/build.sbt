scalaVersion := "2.11.10"

libraryDependencies ++= Seq(
  "com.github.spinalhdl" %% "spinalhdl-core" % "1.3.5",
  "com.github.spinalhdl" %% "spinalhdl-lib"  % "1.3.5",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)

fork := true

scalacOptions += "-feature"
