package n2t

/* test bench */

import spinal.core._
import spinal.sim._
import spinal.core.sim._

object N2TTests {

  class SimTopLevel extends Component {
    val nand = new Nand
    val and  = new And
    val or   = new Or
    val xor  = new Xor

    nand.io.simPublic()
    and.io.simPublic()
    or.io.simPublic()
    xor.io.simPublic()
  }

  def boolToString(b: Bool) = if(b.toBoolean) "1" else "0"

  def main(args: Array[String]): Unit = {
    SimConfig.withWave.compile(new SimTopLevel).doSim { dut =>
      def testGate(name: String, g: Gate)(v: (Boolean, Boolean) => Boolean) = {
        println(s"=== $name ===")
        for(a <- List(false, true)) {
          for(b <- List(false, true)) {
            g.io.a #= a
            g.io.b #= b
            sleep(1)
            println(s"${boolToString(g.io.a)} | " +
              s"${boolToString(g.io.b)} || ${boolToString(g.io.y)} [${if(v(a, b)) "1" else "0"}]")
            assert(g.io.y.toBoolean == v(a, b))
          }
        }
      }

      testGate("NAND", dut.nand)(
        (a:Boolean, b:Boolean) => !(a && b)
      )

      testGate("AND", dut.and)(_ && _)

      testGate("OR", dut.or)(_ || _)

      testGate("XOR", dut.xor) { (a, b) =>
        (a || b) && (!(a && b))
      }

      // SpinalVerilog(new SimTopLevel)
    }
  }
}
