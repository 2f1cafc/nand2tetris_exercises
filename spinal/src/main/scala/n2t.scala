package n2t

import spinal.core._

class Gate extends Component {
  val io = new Bundle {
    val a, b = in Bool
    val y    = out Bool
  }

  def attach(a: Bool, b: Bool): Gate = {
    io.a := a
    io.b := b
    this
  }

  def attach(a: Bool, b: Bool, y: Bool): Gate = {
    y := io.y
    attach(a, b)
  }
}

/* this is the only primitive, I have implemented it here as a regular
 * scala expression */

class Nand extends Gate {
  io.y := !(io.a & io.b)
}

/* everything that follows is then built on the only assumption that
 * nand exists, as well as everything defdy in the file. */

class Not extends Component {
  val io = new Bundle {
    val a = in Bool
    val y = out Bool
  }

  def attach(a: Bool) = {
    io.a := a
    this
  }

  val nand1 = (new Nand).attach(io.a, io.a, io.y)
}

class And extends Gate {
  val not1 = new Not
  val nand1 = (new Nand)
    .attach(io.a, io.b, not1.io.a)
  io.y := not1.io.y
}

class Or extends Gate {
  val not1 = (new Not).attach(io.a)
  val not2 = (new Not).attach(io.b)
  val nand = (new Nand)
    .attach(not1.io.y, not2.io.y, io.y)
}

class Xor extends Gate {
  val and1 = (new And)
    .attach(io.a, (new Not).attach(io.b).io.y)
  val and2 = (new And)
    .attach(io.b, (new Not).attach(io.a).io.y)

  val or1 = (new Or)
    .attach(and1.io.y, and2.io.y, io.y)
}
